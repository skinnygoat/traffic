# Overview

This repository contains code that computes the traffic flow following the Nagel-Schreckenberg (NaSch) Cellular Automata (CA) model. 

After importing `traffic`, call `cellular_automata` to get the density and flow from a simulation. Call `density_flow_plot` to get the fundamental (density-flow) diagram from several simulations at once.

# Parameters

The relevant parameters are the following. Note that periodic boundary conditions (PBCs) are applied throughout the code.

+ `N` (in `cellular_automata` only) Number of vehicles occupying the road
+ `L` Length of the road (in units of 7.5 m)
+ `T` Time duration of the simulation (in units of 1 s)
+ `v_max` Max speed allowed (suggested 5, corresponding to 135 km/h)
+ `p` Randomisation parameter p of NaSch Step 3
+ `p0` Slow-to-start parameter p0 of NaSch Step 0 as modified in Barlovic et al.
+ `plot` Boolean: Whether to plot results
+ `x_init` Initialisation of the vehicle positions: Either `random` (default, random assignment), `jam` (simulating traffic jam environment), `hom` (simulating homogeneous environment)
+ `v_init` Initialisation of the vehicle speeds: Either `max` (default, all cars have max speed on first time frame), `random` (random assignment between 0 and `v_max`)
+ `N_min` (in `density_flow_plot` only): Number of vehicles in the least crowded simulation
+ `N_max` (in `density_flow_plot` only): Number of vehicles in the most crowded simulation
+ `n_repetitions` (in `density_flow_plot` only): Number of simulations to repeat for the same set of parameters
+ `plot_style` (in `density_flow_plot` only): Either `scatter` (default, all simulation results plotted) or `average` (only average flow plotted for each value of the density)

# Literature

+ NaSch original model: Nagel, K., & Schreckenberg, M. A cellular automaton model for freeway traffic, _Journal de Physique I_, **2**, 2221 (1992)
+ Additions to NaSch model to describe metastable states: R. Barlovic, L. Santen, A. Schadschneider, & M. Schreckenberg. Metastable states in cellular automata for traffic flow. _European Physical Journal B_, **5**, 793 (1998)
+ Review: Schadschneider, A. Statistical physics of traffic flow. _Physica A: Statistical Mechanics and Its Applications_, **285**, 101 (2000) 

# Try me's

## Metastable states

Reproduce Fig. 4 in Barlovic et al., Eur. Phys. J. B 5, 793 (1998)

```python
import traffic as t

rho_64, phi_64 = t.density_flow_plot(L=133, N_max=133, n_repetitions=100, p=1./64., p0=1./64., plot=True, T=100, v_max=5, plot_style='average')
rho_64, phi_64 = t.make_average(rho_64, phi_64)

rho_75, phi_75 = t.density_flow_plot(L=133, N_max=133, n_repetitions=100, p=0.75, p0=0.75, plot=True, T=100, v_max=5, plot_style='average') 
rho_75, phi_75 = t.make_average(rho_75, phi_75)

rho_all, phi_all = t.density_flow_plot(L=133, N_max=133, n_repetitions=100, p=1./64., p0=0.75, plot=True, T=100, v_max=5, plot_style='scatter')

rho_jam, phi_jam = t.density_flow_plot(L=133, N_min=8, N_max=23, n_repetitions=100, p=1./64., p0=0.75, plot=True, T=100, v_max=5, plot_style='scatter', x_init='jam')

rho_hom, phi_hom = t.density_flow_plot(L=133, N_min=8, N_max=23, n_repetitions=100, p=1./64., p0=0.75, plot=True, T=100, v_max=5, plot_style='scatter', x_init='hom')

plt.plot(rho_64, phi_64, color='g', label=r'NaSch, $p = \frac{1}{64}$')
plt.plot(rho_75, phi_75, color='r', label=r'NaSch, $p = \frac{3}{4}$')
plt.plot(rho_all,phi_all, 'o', markersize=1.5, color='k', label=r'NaSch+, $p = \frac{1}{64}, p_0 = \frac{3}{4}$')
plt.axvline(x=rho_64[np.argmax(phi_64)], color='g', linestyle='--')
plt.axvline(x=rho_75[np.argmax(phi_75)], color='r', linestyle='--')
plt.plot(rho_jam, phi_jam, 'o', markersize=1.5, color='r')
plt.plot(rho_hom, phi_hom, 'o', markersize=1.5, color='g')
plt.legend()
plt.show()
```
