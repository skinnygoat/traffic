import numpy as np
import matplotlib.pyplot as plt

def cellular_automata(N, L, T, v_max, p, p0=None, plot=False, x_init='random',
                      v_init='max'):
#   par = parse_arguments()

    # Initialise state vectors
    X, V = init(N, L, T, v_max, x_init=x_init, v_init=v_init)

    # Advance in time
    for t in range(T - 1):
        X[:, t+1], V[:, t+1] = step(X[:, t], V[:, t], N, L, v_max, p, p0)

    # Compute density and flow 
    dens = float(N) / float(L) 
    flow = compute_flow(X, L, T)

    # Plot
    if plot:
        space_time_plot(N, L, T, v_max, X, V, p, p0, flow)

    return dens, flow


def density_flow_plot(N_max, L, T, v_max, p, n_repetitions, N_min=2, p0=None, 
                      plot=False, plot_style='scatter', x_init='random', 
                      v_init='max'):
    rho_vec = []
    phi_vec = []

    for N in range(N_min, N_max+1):
        for rep in range(n_repetitions):
            rho, phi = cellular_automata(N, L, T, v_max, p, p0, x_init=x_init,
                                         v_init=v_init)
            rho_vec.append(rho)
            phi_vec.append(phi)

    if plot:
        if plot_style == 'average':
            rho_avg, phi_avg = make_average(rho_vec, phi_vec)
            plt.plot(rho_avg, phi_avg, 'o', markersize=1.5, color='k')
            plt.xlabel(r'Occupancy %')
            plt.ylabel(r'Average Flow (veh/min)')
        elif plot_style == 'scatter':
            plt.plot(rho_vec, phi_vec, 'o', markersize=1.5, color='k')
            plt.xlabel(r'Occupancy %')
            plt.ylabel(r'Flow (veh/min)')
        plt.show()

    return np.array(rho_vec), np.array(phi_vec)


def make_average(rho, phi):
    rho_new, phi_new = zip(*sorted((xVal, np.mean([yVal for a, yVal in zip(rho, phi) if xVal==a])) for xVal in set(rho)))
    return rho_new, phi_new


def init(N, L, T, v_max, x_init='random', v_init='max'):
    X = np.zeros([N, T])
    V = np.zeros([N, T])

    # Initialisation of speeds
    if v_init == 'max':                     # Max speed
        V[:, 0] = v_max * np.ones(N)
    elif v_init == 'random':                # Random speed
        V[:, 0] = np.random.choice(v_max, size=N, replace=True)

    # Initialisation of positions
    if x_init == 'random':                  # Random positions
        X[:, 0] = np.sort(np.random.choice(L, size=N, replace=False)) 
    elif x_init == 'jam':                   # Jam state (speeds overwritten to zero)
        X[:, 0] = np.arange(N)
        V[:, 0] = 0.0 * np.ones(N)
    elif x_init == 'hom':                   # Homogeneous state
        X[:, 0] = np.linspace(0, L, N, endpoint=False, dtype=int)

    return X, V

def space_time_plot(N, L, T, v_max, X, V, p, p0, flow):
    x = X.ravel()
    v = V.ravel()
    t = np.tile(np.arange(T), N)

    # Conversions
    v = v * 7.5 * 3.6
    v_max = v_max * 7.5 * 3.6
    if p0 is None:
        p0 = p

    plt.scatter(x, t, c=v, s=5, cmap='RdYlBu') # viridis
    plt.title(r'$N=%d$; $p=%3.2f$; $p_0=%3.2f$; $\Phi=%2.0f$ veh/min' % (N, p, p0, flow))
    plt.xlabel(r'Space (%3.1f km road)' % (L * 7.5 / 1000))
    plt.ylabel(r'$\leftarrow$ Time (s)')
    ax = plt.gca()
    ax.axes.get_xaxis().set_ticks([])
    ax.invert_yaxis()
    cbar = plt.colorbar()
    cbar.set_label(r'Speed (0 $-$ %d)' % v_max)
    plt.clim(0, v_max)
    plt.show()

def compute_flow(X, L, T):
    displ = np.roll(X, -1, axis=1) - X
    displ[displ < 0] = displ[displ < 0] + L
    tot_displ = displ[:, :-1].sum()         # Space travelled overall by all vehicles
    flow = tot_displ / L / (T / 60.)        # Multiples of the total length per minute
    return flow

def step(x_old, v_old, N, L, v_max, p, p0):

    # Pre-processing
    #   Copy
    x = x_old.copy()
    v = v_old.copy()
    #   Compute headway
    d = np.roll(x, -1) - x
    d[d < 0] = d[d < 0] + L

    # Step 0: Slow-to-start
    ps = np.ones(N) * p
    if p0 is not None:
        ps[v == 0] = p0

    # Step 1: Acceleration
    #   If v(n) < v_max, increase speed by 1
    v[v < v_max] = v[v < v_max] + 1

    # Step 2: Deceleration due to other cars
    #   If d(n) <= v(n), reduce the speed to d(n)-1
    v[v >= d] = d[v >= d] - 1

    # Step 3: Randomisation
    #   If v(n) > 0, the speed is decreased by 1 
    #   with probability ps
    r = np.random.uniform(size=N)
    v[(v > 0) & (r <= ps)] = v[(v > 0) & (r <= ps)] - 1

    # Step 4: Vehicle movement
    #   Move forward according to new velocity
    x = x + v
    x[x >= L] = x[x >= L] - L

    return x, v


    


