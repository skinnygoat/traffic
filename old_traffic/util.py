import numpy as np

def initialise_x(params):
    """ Position initialiser
    If random_init: Initialise randomly sampling from a uniform distribution in 
      [0, circuit_length]
    Otherwise: Initialise uniformly
    """
    if params.random_init_x:
        x = params.circuit_length * np.sort(np.random.rand(params.n_vehs))
    else:
        x = np.linspace(0., circuit_length, num=params.n_vehs)
    return x

def initialise_v(params): 
    """ Velocity initialiser
    If random_init: Initialise randomly sampling from a uniform distribution in [0, v_max/2]
    Otherwise: Initialise to v_max/2.
    """
    if params.random_init_v:
        v = np.sort(np.random.rand(params.n_vehs)) * params.v_max / 2.
    else:
        v = np.ones(params.n_vehs) * params.v_max / 2.0
    return v

def one_time_step(x, v, n_vehs, n_loops, params):
    """ Main timestep function
    Each time this function is called, all the vehicles are (one at a time) moved to their
    next position along the road.
    """
    new_x = x.copy()
    new_v = v.copy()
    for veh in range(n_vehs):
        next_veh = veh + 1 if veh < n_vehs - 1 else 0
        # Update speed
        new_v[veh] = update_v(new_x[veh], new_x[next_veh], new_v[veh], params)
        # Update position
        new_x[veh], new_loop = update_x(new_x[veh], new_x[next_veh], new_v[veh], params)
        n_loops += new_loop
    E = energy(new_x, new_v, params)
    return E, new_x, new_v, n_loops


def update_v(x, x_next, v, params):
    """ Updates the speed of a vehicle
    This function computes the new speed of a vehicle by randomly sampling it from 
    a uniform distribution in [0, v_max]. The kinetic and potential energy variations 
    are then computed. The new speed is accepted or rejected depending on the Metropolis
    algorithm. If rejected, the old speed is kept.
    If a deterministic approach is chosen, the initial speed is always kept.
    The new speed is returned.
    """
    if params.deterministic:
        return v

    # Propose new velocity
    v_candidate = np.random.rand() * params.v_max 
    dv = v_candidate - v
    dv2 = v_candidate**2 - v**2

    # Compute variation in kinetic energy
    # using KE = 1/2 m v**2 
    # thus Δ(KE) = 1/2 m (v_new**2 - v_old**2)
    dE_kin = 0.5 * params.m * dv2

    # Compute variation in potential energy
    # using PE = -W = -F ds = -F v dt 
    # with:
    #   F = m a = m (v_opt - v) / tau
    #   v_opt = v_max Δx**2 / (Δx**2 + D**2)
    #   v_max: A given parameter
    #   Δx: Distance from the vehicle ahead
    #   D: A given parameter
    # thus Δ(PE) = -(F_new v_new - F_old v_old) dt =
    #   = -m ((v_opt - v_new) v_new - (v_opt - v_old) v_old) dt / tau =
    #   = -m (v_opt (v_new - v_old) - v_new**2 + v_old**2) dt / tau =
    #   = -m (v_opt Δv - (v_new**2 - v_old**2)) dt / tau
    dx = x_next - x
    if dx < 0.0:
        dx = params.circuit_length + dx
    v_opt = params.v_max * dx**2 / (dx**2 + params.D**2)
    dE_pot = -params.m * params.dt * (v_opt * dv - dv2) / params.tau

    # Compute variation in total energy
    dE = dE_kin + dE_pot

    # Accept or refuse suggestion using Metropolis
    if dE <= 0.0:
        return v_candidate
    
    xi = np.exp(-dE/params.T)
    rn = np.random.rand()
    if rn <= xi:
        return v_candidate
    return v

def update_x(x, x_next, v, params):
    """ Updates the position of a vehicle
    This function returns the new position of a vehicle, given its previous 
    position, its speed and the time delta.
    """
    displacement = v * params.dt
    avail_space = x_next - x
    if avail_space < 0.:
        avail_space = params.circuit_length + avail_space
    if displacement > avail_space:
        displacement = avail_space
    new_x = x + displacement
    new_loop = 0
    if new_x >= params.circuit_length:
        new_x = new_x - params.circuit_length
        new_loop = 1
    return new_x, new_loop

def energy(x, v, params):
        E = {}
        # Kinetic energy
        E['kinetic'] = np.sum(0.5 * params.m * v**2)
        # Delta x: Distance from vehicle ahead
        Dx = np.roll(x,-1) - x
        Dx[Dx < 0.] = Dx[Dx < 0.] + params.circuit_length
        # Optimal velocity
        v_opt = params.v_max * Dx**2 / (Dx**2 + params.D**2)
        # Dec force
        F_dec = params.m * (v_opt - params.v_max) / params.tau
        # Acc force
        F_acc = params.m * (params.v_max - v) / params.tau
        # Tot force
        F_tot = F_acc + F_dec
        # Potential energy
        E['potential'] = -np.sum(F_tot * v * params.dt)

        return E

