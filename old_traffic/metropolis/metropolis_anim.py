import numpy as np
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation
plt.style.use('seaborn-pastel')

# Initialise data
x_vec = []
x_0 = np.random.uniform()

# Initialise figure
fig, ax = plt.subplots(figsize=(5, 3))
ax.set(xlim=(0, 2))
ax.set(xlabel='x', ylabel='f(x)')

counts, bins, bars = ax.hist([x_0], bins=100, density=True)

def animate(i, x_vec, x_0, bars):
    x_1 = np.random.normal(loc=x_0, scale=0.05)
    if x_1 > 1:
        x_1 = 2 - x_1
    elif x_1 < 0:
        x_1 = -x_1
    
    fact = cust_func(x_1)/cust_func(x_0)
    rand = np.random.uniform()
    if rand <= fact:
        # Accept
        x_0 = x_1
    x_vec.append(x_0)

    _ = [b.remove() for b in bars]
    counts, bins, bars = ax.hist(x_vec, bins=100, density=True)
    return bars


def cust_func(x):
    return np.cos(x*np.pi)+1.
#   return x


ani = FuncAnimation(fig, animate, frames=100, fargs=(x_vec, x_0, bars), repeat=False)
plt.show()



