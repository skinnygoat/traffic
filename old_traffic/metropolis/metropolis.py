import numpy as np
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation
plt.style.use('seaborn-pastel')


def default_func(x):
    return np.cos(x)+1.1


def init(func, boundaries):
    # Initialise data
    x_vec = []
    x = boundaries[0] + np.random.uniform(boundaries[1]-boundaries[0])
    if func is None:
        func = default_func
    return func, x, x_vec


def next_point(x, func, boundaries):
    x_cand = np.random.normal(loc=x, scale=0.05)
    if x_cand > boundaries[1]:
        x_cand = boundaries[0] + (x_cand - boundaries[1])
    elif x_cand < 0:
        x_cand = boundaries[1] - (boundaries[0] - x_cand)
    fact = func(x_cand)/func(x)
    rand = np.random.uniform()
    if rand <= fact:
        return x_cand
    return x


def metropolis(n_cycles, func=None, boundaries=[0, 1], plot_frequency=0):

    if plot_frequency > 0:
        # Initialise figure
        fig, ax = plt.subplots(figsize=(5, 3))
        ax.set(xlim=boundaries)
        ax.set(xlabel='x', ylabel='f(x)')
    
    func, x, x_vec = init(func, boundaries)
    for cyc in np.arange(1, n_cycles+1):
        x = next_point(x, func, boundaries)
        x_vec.append(x)

        if plot_frequency > 0 and cyc % plot_frequency == 0:    
            counts, bins, bars = ax.hist(x_vec, bins=100, color='b', density=True)
            plt.draw()
            ax.set(title=str(cyc)+' draws')
            plt.pause(0.01)
            _ = [b.remove() for b in bars]

    return x_vec
