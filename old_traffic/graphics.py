import numpy as np
import matplotlib.pyplot as plt
plt.style.use('seaborn-pastel')

def plot_time_evolution(t, x, v):
    t = np.array(t)
    x = np.array(x)
    v = np.array(v)

    n_particles = x.shape[1]

    for part in range(n_particles):
        plt.plot(t, x[:,part])

    plt.show()

'''
    for i in range(len(t)):
        fig, ax = plt.subplots()
        
        ax.add_patch(plt.Circle((i, i), 1., color='g', alpha=0.5))
        
        ax.set_aspect('equal', adjustable='datalim')
        ax.plot()
        plt.show()'''

