import argparse
import numpy as np
import util
import graphics

def parse():
    parser = argparse.ArgumentParser(description='Microscopic simulation of traffic.')
    parser.add_argument('-c', dest='n_cycs', type=int, default=10,
            help='Number of cycles')
    parser.add_argument('-N', dest='n_vehs', type=int, default=3,
            help='Number of vehicles')
    parser.add_argument('-rx', dest='random_init_x', action='store_true',
            help='Whether to initialise to random positions')
    parser.add_argument('-rv', dest='random_init_v', action='store_true',
            help='Whether to initialise to random velocities')
    parser.add_argument('-vmax', dest='v_max', type=float, default=130.,
            help='Max velocity (km/h)')
    parser.add_argument('-dt', dest='dt', type=float, default=1.,
            help='Timestep (s)')
    parser.add_argument('-m', dest='m', type=float, default=500.,
            help='Mass (kg)')
    parser.add_argument('-D', dest='D', type=float, default=1.,
            help='D parameter (m)')
    parser.add_argument('-tau', dest='tau', type=float, default=1.,
            help='tau parameter')
    parser.add_argument('-T', dest='T', type=float, default=1.,
            help='Temperature (K)')
    parser.add_argument('-cl', dest='circuit_length', type=float, default=1.,
            help='Length of the circuit (km)')
    parser.add_argument('-det', dest='deterministic', action='store_true',
            help='Whether to move deterministically')
    args = parser.parse_args()
    
    # Conversion
    args.v_max = args.v_max / 3.6
    args.circuit_length = circuit_length * 1000.
    return args

if __name__ == '__main__':
    params = parse()

    # Initialisation
    #   The position and speed of each vehicle are initialised here.
    t = np.zeros((params.n_cycs+1))
    x = np.zeros((params.n_cycs+1, params.n_vehs))
    v = np.zeros((params.n_cycs+1, params.n_vehs))
    n_loops = 0
    x[0, :] = util.initialise_x(params)
    v[0, :] = util.initialise_v(params)
    t[0] = 0

    # Time cycles
    #   At each time cycle, of length dt seconds, each vehicle is moved by one step
    for cyc in range(params.n_cycs):
        E, x[cyc+1, :], v[cyc+1, :], n_loops =\
                util.one_time_step(x[cyc, :], v[cyc, :], params.n_vehs, n_loops, params)
        t[cyc+1] = t[cyc] + params.dt

    print('# loops: %d   time elapsed: %f   flux: %f' % (n_loops, t[params.n_cycs], n_loops/t[params.n_cycs]))
#   graphics.plot_time_evolution(t[:params.n_cycs], x[:params.n_cycs, :], v[:params.n_cycs, :])




